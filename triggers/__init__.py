"""Pipeline triggers."""
from . import baseline_trigger
from . import brew_trigger
from . import patch_trigger

TRIGGERS = {
    'baseline': baseline_trigger,
    'patch': patch_trigger,
    'brew': brew_trigger
}
