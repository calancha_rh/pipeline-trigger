# How to run things

Execute `python3 -m triggers <trigger_name> <options>`. All the triggers except
the brew one are supposed to finish. The brew trigger is supposed to start and
run permanently, as it's listening to UMB messages live.

## Kickstarting the pipeline

The Patchwork pipelines need to know the ID of the last tested event in order
to grab patches newer than that. If this is the first time you are running the
pipeline it's obviously not possible. What the trigger should automatically
grab is unclear in this case -- should it be all existing patches? Last week?
Only the last patch? Different people may expect different outcome, hence we
didn't implement any of these.

Instead we implemented a `--kickstart` option which grabs the last patch series
and submits a dummy pipeline which results shouldn't be sent anywhere. After
this step all the newer patches should be picked up for testing.

## Writing new triggers - tl;dr

* By default, a `<trigger-name>.yaml` file is expected to contain the trigger
  configuration. For testing, you can create any file you want and pass it over
  with the `--config <filename>` option.
* Create a `<trigger-name>_trigger.py` file in the `triggers/` directory. This
  file contains the code needed for transformation of the configuration and any
  external data into the variables passed to the actual pipeline trigger.
* Add a new trigger key to the mapping in `triggers/__init__.py`.
* Write tests for your code !!!

## Some more details, hints and suggestions

* If possible, use the functions in `triggers/utils.py` or various CKI tools
  instead of writing new ones for the same functionality. It makes it easier
  to test and maintain.
* The entry point of the `<trigger-name>_trigger.py` should be a
  `load_triggers` function that takes 3 arguments, which are passed by the
  `main()` function:
  * `gitlab_instance` -- an `gitlab.Gitlab` object
  * `config` -- parsed YAML configuration
  * `kickstart` -- Instead of checking the last tested pipelines before
                   submitting new ones, create a new dummy pipeline instead. The
                   trigger can decide whether to implement this functionality or
                   not, as it may not make sense to be used there.
* `load_triggers()` should return a list of dictionaries, each dictionary fully
  describing the pipeline to trigger. The mandatory parameters are:
  * `cki_project`: The project to trigger the pipelines in, in the
                   `username/repo` format. Should be read from the
                   configuration.
  * `cki_pipeline_branch`: Branch of the `cki_project` which should be used for
                           reference commit creation and pipeline triggering.
                           Should also be read from the configuration.
  * `title`: Title that should be used to identify the commit. Should be created
             based on what's tested.

## List of configuration options required by the trigger system

* `cki_project` - the GitLab project that holds the pipeline to be triggered
* `cki_pipeline_project` - the GitLab subproject that holds the pipeline to be
  triggered. The complete project is built from the user/project given in the
  `GITLAB_PARENT_PROJECT` environment variable. If this variable is used,
  `cki_project` should not be specified. At the moment, this is only supported
  by the baseline trigger.
* `cki_pipeline_branch` - the branch in the `cki_project` that should receive
  a commit to start a pipeline run. This variable is not a reliable indicator
  for anything and should not be used inside the pipeline!
* `event_id` - the last tested series in Patchwork via their event number; used
  by the patchwork pipeline trigger to know where to begin looking for new
  patches later.
* `name` - the pipeline name (example: `upstream-stable`)
* `patchwork_project` - the project linkname for the kernel tree in Patchwork
* `patchwork_url` - URL to the Patchwork instance to use
* `title` - the title of the commit made into the pipeline repository to
  identify the testing
* `branch` - The branch to check out after the repo is cloned

For Brew-trigger specific fields, check out the configuration example below.

## Environment variables

* `CKI_DEPLOYMENT_ENVIRONMENT` - if not `production`, variables will be cleaned
  for retriggering. Emails are removed, the pipeline is marked as a retrigger,
  latest ystream that passed testing is used, and last patch tracking,
  Patchwork variables, and Beaker are disabled.
* `GITLAB_URL` - info for GitLab instance to trigger pipeline on
* `GITLAB_TOKENS` - URL/environment variable pairs of GitLab instances and private tokens
* `GITLAB_TOKEN` - GitLab private tokens as configured in `GITLAB_TOKENS` above
* `GITLAB_PARENT_PROJECT` - user/project prepended to `cki_pipeline_project`

## Format and dummy examples of currently supported config files

Baseline:

```yaml
name:
  git_url: url
  .branches:
    - branch-to-test
    - another-branch-to-test
  cki_project: CKI-project/cki-pipeline
  cki_pipeline_branch: cki-branch
  any-other-vars: here

name2:
  git_url: url2
  .branches:
    - branch-to-test
    - another-branch-to-test
  cki_pipeline_project: cki-pipeline
  cki_pipeline_branch: cki-branch-2
  any-other-vars: here
```

Brew/Koji/COPR:

```yaml
.amqp:
  .message_topics:
    - list-of-topics-here
    - 'topic://VirtualTopic.eng.brew.task.closed.>'
  .receiver_urls:
    - 'amqps://receiver:port'
    - 'amqps://receiver2:port'
  .cert_path: filepath
  server_url: http://koji-example.org/kojihub
  web_url: http://koji-example.org/koji
  top_url: http://pkgs.koji-example.org/
.zmq:
  .message_topics:
    - 'org.fedoraproject.prod.copr.build.end'
    - 'org.fedoraproject.prod.buildsys.build.state.change'
  .receiver_urls:
    - 'tcp://hub:port'
  server_url: http://koji-example.org/kojihub
  web_url: http://koji-example.org/koji
  top_url: http://pkgs.koji-example.org/
.default:
  any-global-variables: here
name-fedora:
  .extends: .zmq
  .test_scratch: 'False'
  .coprs:
    - username/copr
    - another/one
  rpm_release: 'fc30'
  package_name: kernel
  cki_project: CKI-project/brew-pipeline
  cki_pipeline_branch: fedora
  any-pipeline-specific-variables: here
name-fedora-rt:
  .extends: .zmq
  .test_scratch: 'True'
  .owners:
    - only-trigger-for-this-user
    - and-this-user
  rpm_release: 'fc30'
  package_name: kernel-rt
  cki_project: CKI-project/brew-pipeline
  cki_pipeline_branch: fedora
```

Patchwork:

```yaml
name:
  git_url: url
  branch: branch
  cki_project: CKI-project/cki-pipeline
  cki_pipeline_branch: cki-branch
  patchwork:
    url: http://patchwork.example.org
    project: example-project
  any-other-vars: here
```

## Override/add variables in all pipelines triggered

If you need override or add a variable into all pipelines, you can use the
`--variables` parameter. For instance, if you want to avoid submitting jobs to
beaker, write down `--variables skip_beaker=true`, it will add into all
pipelines triggered the variable `skip_beaker = true`.

## Using inheritance in the pipeline configuration

For the triggers that support multiple trigger configurations, basic
inheritance and defaults modeled after the
[default](https://docs.gitlab.com/ce/ci/yaml/#global-defaults) and
[extends](https://docs.gitlab.com/ce/ci/yaml/#extends) keywords in
the [GitLab CI/CD job descriptions](https://docs.gitlab.com/ce/ci/yaml) can be
used.

* trigger configurations with a name starting with a dot (`.`) are not
  processed, but can be used for inheritance
* the `.default` trigger configuration will be inherited by all trigger
  configurations
* the configuration option `.extends` followed by a name or list of names
  specifies trigger configuration(s) to inherit from
* configuration options with a name starting with a dot (`.`) are used to
  configure the trigger, but are NOT passed to the pipeline code as trigger
  variables

Partial example:

```yaml
.zmq:
  .cert_path: '/etc/cki/umb/cki-bot-prod.pem'
  server_url: 'https://server.org/'
.default:
  mail_from: CKI Project <cki-project@redhat.com>
  var_to_override: 'False'
.send_upstream:
  var_to_override: 'True'
fedora-30:
  .extends: .zmq
  package_name: kernel
  rpm_release: 'fc31'
fedora-31:
  .extends: [fedora-30, .send_upstream]
  rpm_release: 'fc31'
  .coprs:
    - username
```

## Brew trigger metrics

If `CKI_METRICS_ENABLED` is `true`, prometheus metrics are exposed on the `CKI_METRICS_PORT` port.

The exposed data is the following:

| Name                           | Type      | Description                                               |
|--------------------------------|-----------|-----------------------------------------------------------|
| `message_received`             | Counter   | Number of messages received                               |
| `message_process_time_seconds` | Histogram | Time spent processing a message                           |
| `pipeline_triggered`           | Counter   | Number of pipelines triggered                             |
| `processing_load`              | Summary   | Normalized indicator of the load of the messages receiver |
